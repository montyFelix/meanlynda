const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', (req, res, next) => {
  if (req.user) {
    return res.redirect('/orders');
  }
  let vm = {
    title: 'Login',
    error: req.flash('error')
  };
  res.render('index', vm);
});

module.exports = router;
