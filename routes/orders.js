const express = require('express');
const router = express.Router();
const restrict = require('../auth/restrict');
const orderService = require('../services/order-service');


router.get('/', restrict, (req, res, next) => {
  let vm = {
    title: 'Plase an order',
    firstName: req.user ? req.user.firstName : null
  };
  res.render('orders/index', vm);
});

router.get('/api/restaurants', restrict, (req, res, next) => {
  orderService.searching((err, restaurants) => {
    if (err) {
      return res.status(500).json({error: 'Failed to retrieve restaurants'});
    }
    res.json(restaurants);
  });
});

module.exports = router;
