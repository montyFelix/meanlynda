const express = require('express');
const router = express.Router();
const passport = require('passport');
const userService = require('../services/user-service');
const config = require('../config');

/* GET users listing. */
router.get('/', (req, res, next) => {
  res.send('respond with a resource');
});

/* GET user listing. */
router.get('/create', (req, res, next) => {
  let vm = {title: 'Create an account' };
  res.render('users/create', vm);
});

router.post('/create', (req, res, next) => {
  userService.addUser(req.body, (err) => {
    if (err) {
      console.log(err.errors);
      var vm = {
        title: 'Create an account',
        input: req.body,
        error: err
      };
      delete vm.input.password;
      return res.render('users/create', vm);
    }
    req.login(req.body, (err) => {
      res.redirect('/orders');
    });
  });
});

router.post('/login',
  (req, res, next) => {
    if (req.body.rememberMe) {
      req.session.cookie.maxAge = config.cookieMaxAge;
    }
    next();
  },
  passport.authenticate('local',
  {
    failureRedirect: '/',
    successRedirect: '/orders',
    failureFlash: 'Invalid credentials'
  }
));

router.get('/logout', (req, res, next) => {
  req.logout();
  req.session.destroy();
  res.redirect('/');
});

module.exports = router;
