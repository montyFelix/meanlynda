'use strict';

angular.module('app', ['ngRoute']).config(['$routeProvider', ($routeProvider) => {
  $routeProvider.otherwise({redirectTo:'/restaurants'});
}]);
