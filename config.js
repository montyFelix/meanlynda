let config = {};

config.mongoUri = 'mongodb://localhost:27017/rtr';
config.cookieMaxAge = 30 * 24 * 3600 * 1000;
config.orderxKey = 'e73791eeaa243dc49d5880b5a02bf3ae';

module.exports = config;
