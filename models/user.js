const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let userService = require('../services/user-service');

const userSchema = new Schema({
  firstName: {type:String, required: 'Please enter First Name'},
  lastName: {type:String, required: 'Please enter Last Name'},
  roomNumber: {type:Number, required: 'Please enter valid room number >=100', min:[100, 'Invalid room number']},
  email: {type:String, required: 'Please enter email'},
  password: {type:String, required: 'Please enter password'},
  created: {type: Date, default: Date.now()}
});

userSchema.path('email').validate((value, next) => {
  userService.findUser(value, (err, user) => {
    if (err) {
      console.log(err);
      return next(false);
    }
    next(!user);
  });
}, 'That email is already in use');

let User = mongoose.model('User', userSchema);

module.exports = {
  User: User
};
