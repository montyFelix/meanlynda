module.exports = () => {
  let passport = require('passport');
  let passportLocal = require('passport-local');
  const bcrypt = require('bcrypt');
  let userService = require('../services/user-service');

  passport.use(new passportLocal.Strategy({usernameField: 'email'}, (email, password, next) => {
    userService.findUser(email, (err, user) => {
      if (err) {
        return next(err);
      }
      if (!user) {
        return next(null, null);
      }
      bcrypt.compare(password, user.password, (err, same) => {
        if (err) {
          return next(err);
        }
        if (!same) {
          return next(null, null);
        }
        next(null, user);
      });
    });
  }));

  passport.serializeUser((user, next) => {
    next(null, user.email);
  });

  passport.deserializeUser((email, next) => {
    userService.findUser(email, (err, user) => {
      next(err, user);
    });
  });
};
