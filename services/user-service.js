const bcrypt = require('bcrypt');
const User = require('../models/user').User;

exports.addUser = (user, next) => {
  bcrypt.hash(user.password, 10, (err, hash) => {

    if (err) {
      return next(err);
    }

  let newUser = new User({
    firstName: user.firstName,
    lastName: user.lastName,
    roomNumber: user.roomNumber,
    email: user.email.toLowerCase(),
    password: hash
  });

  newUser.save((err) => {
    if (err) {
      return next(err);
    }
    next(null);
  });
});
};

exports.findUser = (email, next) => {
  User.findOne({email: email.toLowerCase()}, (err, user) => {
    next(err, user);
  });
};
