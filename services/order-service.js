const zomato = require('zomato');
const config = require('../config');

const api = zomato.createClient({
  userKey: config.orderxKey,
});

exports.searching = (next) => {
  let args = {
    city_id:"280",//location id
    count:"10", // number of maximum result to display
  };

  api.getCollections(args, (err, restaurants) => {
    if (!err) {
      console.log(config.orderxKey);
      console.log(restaurants);
      next(null, restaurants);
    } else {
      console.log(err);
      return next(err);
    }
  });
};
